import axios from "axios";
import { localUserService } from "./localService";

export const BASE_URL = "https://movienew.cybersoft.edu.vn/";
export const TokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U";

export const configHeader = () => {
  return {
    TokenCybersoft: TokenCybersoft,
    Authorization: "bearer " + localUserService.get()?.accessToken,
    // optional chainning
  };
};
export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeader(),
});
// axios instance
