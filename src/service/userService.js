import axios from "axios";
import { BASE_URL, configHeader, https } from "./config";

export const userServ = {
  postLogin: (loginForm) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
    //   method: "POST",
    //   headers: configHeader(),
    //   data: loginForm,
    // });
    return https.post("/api/QuanLyNguoiDung/DangNhap",loginForm)
  },
};
