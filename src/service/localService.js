export const localUserService = {
  get: () => {
    let dataJSON = localStorage.getItem("USER_INFOR");
    return JSON.parse(dataJSON);
    // return null || object
    // null, undefine, "", false, NaN,0 -> falsty
  },
  set: (userInfo) => {
    let dataJSON = JSON.stringify(userInfo);
    localStorage.setItem("USER_INFOR", dataJSON);
  },
  remove: () => {
    localStorage.removeItem("USER_INFOR")
  },
};
