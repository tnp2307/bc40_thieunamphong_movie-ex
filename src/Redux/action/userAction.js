import { message } from "antd";
import { localUserService } from "../../service/localService";
import { userServ } from "../../service/userService";
import { USER_LOGIN } from "../Constant/userConstant";

export const setLoginActions = (value) => {
  // value den tu response axios
  return {
    type: USER_LOGIN,
    payload: value,
  };
};

// reduxThunk
export const setLoginActionService = (value, onCompleted) => {
  // value den tu the form cua antd
  return (dispatch) => {
    userServ
      .postLogin(value)
      .then((res) => {
        localUserService.set(res.data.content);
        onCompleted();
        dispatch(setLoginActions(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

// redux thunk ¬ goi api trong action
