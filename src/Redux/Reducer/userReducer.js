import { localUserService } from "../../service/localService";
import { USER_LOGIN } from "../Constant/userConstant";

// rxreducer
const initialState = {
  isLogin: false,
  userInfor: localUserService.get(),
};

export let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN: {
      return { ...state, userInfor: payload };
    }
    default:
      return state;
  }
};
