import { createSlice } from "@reduxjs/toolkit";
import { localUserService } from "../service/localService";
// rxslice
let initialState = {
  userInfor: localUserService.get(),
  // userInfor: "122123",
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLoginAction: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});

export const { setLoginAction } = userSlice.actions;
export default userSlice.reducer;
