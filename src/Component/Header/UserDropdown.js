import React from "react";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";

const UserDropdown = (props) => (
  <Dropdown
    menu={{
      items: [
        { label: props.logoutBtn, key: "1" },
        { label: (<span>Cap nhat tai khoan</span>), key: "2" },
      ],
    }}    trigger={['click']}

   
  >
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        {props.user.hoTen}
        <DownOutlined />
      </Space>
    </a>
  </Dropdown>
);
export default UserDropdown;
