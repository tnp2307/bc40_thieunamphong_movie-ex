import React from "react";
import {  useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserService } from "../../service/localService";
import UserDropdown from "./UserDropdown";

export default function UserMenu() {
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogout = () => {
    localUserService.remove();
    window.location.reload();
    // window.location.href = "/login";
  };
  let renderContent = () => {
    let buttonCss = "px-5 py-2 border-2 border-black rounded";
    if (userInfor) {
      // da dang nhap
      return (
        <>
          <UserDropdown user = {userInfor} logoutBtn={<button onClick={handleLogout} className={buttonCss}>Logout</button>} />
          
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className={buttonCss}>Login</button>
          </NavLink>

          <button className={buttonCss}>Sign up</button>
        </>
      );
    }
  };
  return (
    <div className="space-x-5 flex">
      {renderContent()}
    </div>
  );
}
