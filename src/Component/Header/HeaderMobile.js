import React from "react";
import UserMenu from "./UserMenu";
import { NavLink } from "react-router-dom";

export default function HeaderMobile() {
  return (
    <div>
      <div className="h-20 shadow w-full bg-gray-400">
        <div className="container mx-auto h-full flex items-center justify-between">
          <NavLink to="/homepage" className=" font-medium text-2xl text-red-500 animate-pulse">
            CyberFlix{" "}
          </NavLink>
          <span className=" font-medium text-2xl text-red-500">CyberFlix </span>
          <span className=" font-medium ">
            <UserMenu />{" "}
          </span>
        </div>
      </div>
    </div>
  );
}
