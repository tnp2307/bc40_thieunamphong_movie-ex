import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../../service/userService";
import { localUserService } from "../../service/localService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../../Redux/Constant/userConstant";
import Lottie from "lottie-react";
import bg_animate from "../../asset/login_animated.json";
import {
  setLoginActions,
  setLoginActionService,
} from "../../Redux/action/userAction";
import { setLoginAction } from "../../toolkit/userSlice";

const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log(values);
    userServ
      .postLogin(values)
      .then((res) => {
        message.success("Login successful")
        //   luu thong tin user vao local storage
        localUserService.set(res.data.content);
        // chuyen thong tin user infor vao reducer
        dispatch(setLoginAction(res.data.content));
        // chuyen huong user toi homepage
        navigate("/homepage");
        console.log(res);
      })
      .catch((err) => {
        message.error("Dang nhap that bai");
        console.log(err);
      });
  };
// Redux tool kit
  const onFinishToolKit = (values) => {
    console.log(values);
    userServ
      .postLogin(values)
      .then((res) => {
        message.success("Login successful")
        //   luu thong tin user vao local storage
        localUserService.set(res.data.content);
        // chuyen thong tin user infor vao reducer
        dispatch(setLoginAction(res.data.content))
        // chuyen huong user toi homepage
        navigate("/homepage");
        console.log(res);
      })
      .catch((err) => {
        message.error("Dang nhap that bai");
        console.log(err);
      });
  };
  // redux thunk
  const onFinishThunk = (value) => {
    let onSuccess=() => {    
      navigate("/homepage");
      message.success("Login successful")
     }  
    // callback
    dispatch(setLoginActionService(value,onSuccess));
    
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="h-screen w-screen flex bg-orange-500 justify-center items-center">
      <div className="container mx-auto p-5 bg-white rounded flex items-center">
        <div className="w-1/2 desktop:w-full h-full">
          <Lottie className="w-1/2 desktop:w-full" animationData={bg_animate} />
        </div>
        <div className="w-1/2 h-full">
          {" "}
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 32,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishToolKit}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="horizontal"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 0,
                span: 16,
              }}
            ></Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 4,
              }}
              className=" flex justify-center items-center"
            >
              <Button
                className="bg-orange-500 text-white hover:text-black hover:border-hidden"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>{" "}
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
//redux toolkit = redux + redux thunk
// redux react-redux redux thunk