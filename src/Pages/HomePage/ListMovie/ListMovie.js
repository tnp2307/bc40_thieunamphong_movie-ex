import React from 'react'
import { Desktop, Mobile } from '../../../Layout/Reponsive'
import ListMovieDesktop from './ListMovieDesktop'
import ListMovieMobile from './ListMovieMobile'

export default function ListMovie() {
  return (
    <div>
      <Desktop>
        <ListMovieDesktop/>
      </Desktop>
      <Mobile>
        <ListMovieMobile/>
      </Mobile>
    </div>
  )
}
