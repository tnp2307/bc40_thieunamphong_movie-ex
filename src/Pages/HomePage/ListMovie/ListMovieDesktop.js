import React, { useEffect, useState } from 'react'
import { movieService } from '../../../service/movieService'
import ItemMovie from './ItemMovie';

export default function ListMovieDesktop() {
    const [movies, setMovies] = useState([])
    useEffect(()=>{
        movieService.getMovieList().then((res) => {
                console.log(res);
                setMovies(res.data.content)

              })
              .catch((err) => {
               console.log(err);
              });
    },[]
    )
  return (
    <div className='container mx-auto grid grid-cols-5 gap-10'>
        {movies.map((item)=>{
            return <ItemMovie data={item} key={item.maPhim} />
        })}
    </div>
  )
}
