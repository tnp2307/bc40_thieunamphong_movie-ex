import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Component/spinner/Spinner";
import { adminServ } from "../../service/adminservice";
import { movieService } from "../../service/movieService";
import { setLoadingOff, setLoadingOn } from "../../toolkit/spinnerSlice";
import { headerMovie, headerUser } from "../AdminUserPage/ulti";


export default function AdminMoviePage() {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    movieService
      .getMovieList()
      .then((res) => {
        console.log(res);
        dispatch(setLoadingOff());
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Spinner />
      <Table columns={headerMovie} dataSource={movies} />
    </div>
  );
}
