import React from "react";
import { useEffect, useState } from "react";
import { movieService } from "../../service/movieService";
import { Tabs } from "antd";
import ItemMovie from "../HomePage/ListMovie/ItemMovie";
import ItemTabMovie from "./ItemTabMovie";
const onChange = (key) => {
  console.log(key);
};

export default function TabMovie() {
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        console.log(res);
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return heThongRap.map((item) => {
    
      return {
        key: item.maHeThongRap,
        label: <img className="h-16" src={item.logo} alt="" />,
        children: (
          <Tabs
          style={{height:500}}
            tabPosition="left"
            defaultActiveKey="1"
            items={item.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: <div >
                    {cumRap.tenCumRap}</div>,
                children: (
                  <div style={{height:500}} className="overflow-y-scroll">{cumRap.danhSachPhim.slice(0,9).map((phim) => { return <ItemTabMovie phim={phim} key={phim.maPhim} />  })}</div>
                    
                )
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div className="container">
      {" "}
      <Tabs
        style={{height:500}}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
