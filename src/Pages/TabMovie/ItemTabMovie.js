import React, { memo } from "react";
import moment from "moment";
export default function ItemTabMovie({ phim }) {
  
  return (
    <div className="flex p-5 ">
      <img src={phim.hinhAnh} className="w-28 h-36" alt="" />
      <div>
        <h3 className="font-medium text-xl">{phim.tenPhim}</h3>
        <div className="grid grid-cols-3  gap-5">
          {phim.lstLichChieuTheoPhim.splice(0, 9).map((item) => {
            return (
              <span className="rounded p-2 bg-red-500 text-white">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
