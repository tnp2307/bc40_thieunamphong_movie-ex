import axios from "axios";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieService } from "../../service/movieService";
import { Progress } from "antd";
import TabListBySchedule from "./TabListBySchedule/TabListBySchedule";

export default function DetailPage() {
  let { id } = useParams();
  // lấy id của movie từ route của page
  const [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieService.getDetailMovie(id);
        setMovie(result.data.content);
        console.log(result);
      } catch (error) {
        console.log(
          "🚀 ~ file: DetailPage.js:14 ~ fetchDetail ~ error:",
          error
        );
      }
    };
    fetchDetail();
  }, []);
  //  asycn await ~then catch
  return (
    <div className=" ">
      <div className="flex">
        <img src={movie.hinhAnh} className="w-1/3" alt="" />
        <div className="flex-grow" >
          <h2>{movie.tenPhim}</h2>
          <h2>{movie.moTa}</h2>
          <Progress percent={movie.danhGia * 10} />
        </div>
      </div>
      <NavLink
        className="rounded px-5 py-2 bg-red-500 text-white font-medium "
        to={`/booking/${id}`}
      >
        Mua Vé
      </NavLink>
      <TabListBySchedule id={id} />
    </div>
  );
}
