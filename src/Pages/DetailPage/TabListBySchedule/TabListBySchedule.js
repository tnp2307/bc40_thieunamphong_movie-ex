import React, { useEffect, useState } from "react";
import { movieService } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabLichChieu from "./ItemTabLichChieu";
const onChange = (key) => {
    console.log(key);
  };

export default function TabListBySchedule(props) {
  const [heThongRap, setHeThongRap] = useState([]);
  useEffect(() => {
    movieService
      .getScheduleByMovie(props.id)
      .then((res) => {
        console.log(res);
        setHeThongRap(res.data.content.heThongRapChieu)
      })
      .catch((err) => {
        console.log(err);
      });
  },[]);
  
  const renderSchedule = () => {
    return heThongRap.map((item)=>{
        return {
            key: item.maHeThongRap,
            label: <img className="h-16" src={item.logo} alt="" />,
            children: (
                <Tabs
                style={{height:300}}
                  tabPosition="left"
                  defaultActiveKey="1"
                  items={item.cumRapChieu.map((cumRap) => {
                    return {
                      key: cumRap.tenCumRap,
                      label: <div >
                          {cumRap.tenCumRap}</div>,
                      children: (
                        <div style={{height:300}} className=" content overflow-y-scroll">{cumRap.lichChieuPhim.slice(0,20).map((phim) => { return <ItemTabLichChieu phim={phim} key={phim.maLichChieu} />  })}</div>
                          
                      )
                    };
                  })}
                  onChange={onChange}
                />
            ),
          };
    })
  };
  return (
    
    <div className="container">
      {" "}
      <Tabs
        style={{ height: 400 }}
        tabPosition="top"
        defaultActiveKey="1"
        
        items={renderSchedule()}
        onChange={onChange}
      />
    </div>
  );
}
