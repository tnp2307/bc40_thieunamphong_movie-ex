import moment from "moment";
import React from "react";

export default function ItemTabLichChieu(props) {
  return (
    <button className=" m-4 rounded px-4 py-2 bg-purple-300 text-white">
      <h2>{props.phim.tenRap}</h2>
      {moment(props.phim.tenRap).format("DD/MM/YYYY")}
      <br/>
      {moment(props.phim.tenRap).format("hh:mm")}
    </button>
  );
}
