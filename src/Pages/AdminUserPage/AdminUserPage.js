import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Component/spinner/Spinner";
import { adminServ } from "../../service/adminservice";
import { setLoadingOff, setLoadingOn } from "../../toolkit/spinnerSlice";
import { headerUser } from "./ulti";

export default function AdminUserPage() {
  const [user, setUser] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    adminServ
      .getUserList()
      .then((res) => {
        console.log(res);
        dispatch(setLoadingOff());
        setUser(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Spinner />
      <Table columns={headerUser} dataSource={user} />
    </div>
  );
}
