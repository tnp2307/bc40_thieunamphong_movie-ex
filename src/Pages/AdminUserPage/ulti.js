import { Button, message, Tag } from "antd";
import { adminServ } from "../../service/adminservice";

let handleXoaTaiKhoan = (taikhoan) => {
  console.log(taikhoan);
};
export const headerUser = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ và tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số điện thoại",
    dataIndex: "soDT",
    key: "soDT",
  },
  {
    title: "Mật khẩu",
    dataIndex: "matKhau",
    key: "matKhau",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (type) => {
      if (type == "KhachHang") {
        return <Tag color="blue">Khách hàng</Tag>;
      } else {
        return <Tag color={"orange"}>Quản trị</Tag>;
      }
    },
  },
  {
    title: "Thao tác",

    key: "thaoTac",
    render: (_, user) => {
      return (
        <Button
          onClick={() => {
            adminServ
              .deleteUser(user.taiKhoan)
              .then((res) => {
                console.log(res);
                message.success("Xóa thành công")
              })
              .catch((err) => {
                console.log(err);
                message.error(err.response.data.content)
              });
          }}
          type="primary"
          danger
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            class="w-6 h-6"
          >
            <path d="M10.375 2.25a4.125 4.125 0 100 8.25 4.125 4.125 0 000-8.25zM10.375 12a7.125 7.125 0 00-7.124 7.247.75.75 0 00.363.63 13.067 13.067 0 006.761 1.873c2.472 0 4.786-.684 6.76-1.873a.75.75 0 00.364-.63l.001-.12v-.002A7.125 7.125 0 0010.375 12zM16 9.75a.75.75 0 000 1.5h6a.75.75 0 000-1.5h-6z" />
          </svg>
        </Button>
      );
    },
  },
];

export const headerMovie = [
  {
    title: "Số id",
    dataIndex: "maPhim",
    key: "maPhim",
  },
  {
    title: "Tên Phim",
    dataIndex: "tenPhim",
    key: "tenPhim",
  },
  {
    title: "Mô tả",
    dataIndex: "moTa",
    key: "moTa",
    render: (moTa) => {
      return <p className="w-60 text-gray-600">{moTa}</p>;
    },
  },
  {
    title: "Hình ảnh",
    dataIndex: "hinhAnh",
    key: "hinhAnh",
    render: (img) => {
      return <img className="w-40" src={img} alt />;
    },
  },
];
