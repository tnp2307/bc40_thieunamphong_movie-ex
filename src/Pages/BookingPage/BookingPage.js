import React from 'react'
import { useParams } from 'react-router-dom'

export default function BookingPage() {
    let {id} = useParams()
    return (
    <div>
        Booking {id}
    </div>
  )
}
