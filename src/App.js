import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Spinner from "./Component/spinner/Spinner";
import AdminLayout from "./Layout/AdminLayout";
import Layout from "./Layout/Layout";

import BookingPage from "./Pages/BookingPage/BookingPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import { adminRoutes } from "./Route/AdminRoutes";

function App() {
  return (
    <div>
      <Spinner/>
      {/* <AdminLayout/> */}
      <BrowserRouter>
        <Routes>
          <Route path="/homepage" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route
            path="/booking/:id"
            element={<Layout Component={BookingPage} />}
          />
          <Route path="*" element={<Layout Component={NotFoundPage} />} />
          {adminRoutes.map(({ url, component }) => {
            return <Route key={url} path={url} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

//404 page codepen
export default App;
